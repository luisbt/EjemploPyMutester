import unittest
from clase import Persona
from datetime import datetime as dt


class TestMiClase(unittest.TestCase):
    def setUp(self):
        self.persona1 = Persona("Alejandra", "123456789", dt(1980, 1, 1))

    def test_algo(self):
        self.assertEqual(self.persona1.obtener_nombre(), "Alejandra")


if __name__ == '__main__':
    unittest.main()
