# Ejemplo PyMutester

Para que este ejemplo funcione se instala nose y pymutester con pip

```
$ sudo pip install nose
```

```
$ sudo pip install pymutester
```
## Ejemplo

```
nosetest --with-coverage --with-xunit EjemploPyMutester/
```

se ejecuta la prueba unitaria con nosetest y cuando pasan se corren otra vez con mutant-nosetests
para eso se pone uno o mas --mutant-path que especifica la carpeta con el codigo a mutar

```
$ mutant-nosetest --mutant-path /project/myapp tests/mock_tests
```

en este caso:

```
$ mutant-nosetests --mutant-path EjemploPyMutester EjemploPyMutester/testmiclase.py
```
