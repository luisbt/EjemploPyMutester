# Clase
import random


class Persona:

    # Atributo de clase (Estatico)
    conteo = 0

    # Constructor

    def __init__(self, nombre, cedula, fecha_nacimiento):

        # Atributo
        self.nombre = nombre
        self.cedula = cedula
        self.fecha_nacimiento = fecha_nacimiento

        # Atributo "privado"
        self.__aleatorio = random.randint(0, 100)

    def establecer_nombre(self, nombre):

        # Atributo
        self.nombre = nombre

    def obtener_nombre(self):
        return self.nombre

    def establecer_cedula(self, cedula):
        self.cedula = cedula

    def obtener_cedula(self):
        return self.cedula

    def establecer_fecha_nacimiento(self, fecha_nacimiento):
        self.fecha_nacimiento = fecha_nacimiento
